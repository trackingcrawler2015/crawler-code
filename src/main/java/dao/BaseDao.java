package dao;

import java.io.File;
import java.security.CodeSource;
import java.sql.SQLException;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import utils.FileUtils;
import model.CCY;
import model.crawler.RequestFail;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

public abstract class BaseDao {
	String dbURL = "/resources/data/%s";
	ConnectionSource connectionSource = null;
	String dbName;
	Dao<RequestFail, String> reqFailDao;
	final static Logger logger = LogManager.getLogger(BaseDao.class);

	public ConnectionSource getConnection() {
		//logger.info("getConnection");
		String file=null;
		try {

			ClassLoader classLoader = getClass().getClassLoader();
			// File f = new
			// File(classLoader.getResource(settingPath).getFile());
			dbName = this.getClass().getSimpleName().replace("Dao", "");
			file = FileUtils.getFileSqlite(dbName);
			//logger.info(file);
			connectionSource = new JdbcConnectionSource(file);			


			reqFailDao = DaoManager.createDao(connectionSource,
					RequestFail.class);
		} catch (SQLException e) {
			logger.info("abc"+file);
			logger.error(e.getMessage());
			
		} catch (Exception e){
			logger.error(e.getMessage());
			
		}
		return connectionSource;
	}

	public ConnectionSource getConnection(String dbURL) {
		try {
			connectionSource = new JdbcConnectionSource(dbURL);
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
		return connectionSource;

	}

	public void close() {
		if (connectionSource != null) {
			try {
				connectionSource.close();
			} catch (SQLException e) {
				logger.error(e.getMessage());
			}
		}
	}

	public void insertReqFail(RequestFail requestFail) {

		try {
			getConnection();
			reqFailDao.create(requestFail);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			close();
		}

	}

	public List<RequestFail> getListReqFailed() {
		List<RequestFail> listReq = null;
		try {
			getConnection();
			listReq = reqFailDao.queryForAll();
		} catch (Exception e) {
			logger.error(e.getMessage());
		} finally {
			close();
		}
		return listReq;
	}

	public void deleteReqFail(RequestFail requestFail) {

		try {
			getConnection();
			reqFailDao.delete(requestFail);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			close();
		}

	}

}
