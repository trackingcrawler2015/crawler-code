package dao;

import java.sql.SQLException;
import java.util.List;

import model.CCY;
import model.CurrencyRate;
import model.crawler.RequestFail;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.table.TableUtils;

public class A01Dao extends BaseDao {
	CommonDAO commonDAO;

	String dbName;
	Dao<CurrencyRate, String> ccyRateDao;

	public A01Dao() {
		commonDAO = new CommonDAO();

	}

	private void open() {
		try {
			connectionSource = getConnection();
			ccyRateDao = DaoManager.createDao(connectionSource,
					CurrencyRate.class);
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public void createTables() {

		try {
			open();
			TableUtils.dropTable(connectionSource, CurrencyRate.class, true);
			TableUtils.createTable(connectionSource, CurrencyRate.class);
			//TableUtils.dropTable(connectionSource, RequestFail.class, true);
			TableUtils.createTableIfNotExists(connectionSource, RequestFail.class);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close();
		}
	}

	public void createCurrencyRate(CurrencyRate ccyRate) {
		try {
			open();
			ccyRateDao.create(ccyRate);			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			close();
		}
	}

	public void createCurrencyRateList(List<CurrencyRate> listCurrencyRate) {
		try {

			open();
			
			ccyRateDao.setAutoCommit(connectionSource.getReadWriteConnection(), false);
			for (CurrencyRate ccyRate : listCurrencyRate) {
				ccyRateDao.create(ccyRate);
			}
			ccyRateDao.commit(connectionSource.getReadWriteConnection());

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			close();
		}
	}

	public List<CurrencyRate> getListCCYRate() {
		List<CurrencyRate> listCCYRate = null;
		try {
			open();
			listCCYRate = ccyRateDao.queryForAll();
		} catch (Exception e) {
			logger.error(e.getMessage());
		} finally {
			close();
		}
		return listCCYRate;
	}

}
