package exporters.converter;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import spiders.A01;
import utils.DateUtils;
import model.CCY;
import model.CurrencyRate;
import exporters.oracle.model.OandaCurrencyRates;

public class Converter {

	public static OandaCurrencyRates convertCCYRate(CurrencyRate ccyRate) {
		OandaCurrencyRates objInfo = new OandaCurrencyRates();
		objInfo.setIsoCurrencyCode(ccyRate.getQuoteCurrency());
		objInfo.setRate(new BigDecimal(ccyRate.getRate()));
		objInfo.setRateDate(DateUtils.getDateFromString(ccyRate.getDate(),
				A01.datePattern));
		return objInfo;
	}

	public static List<OandaCurrencyRates> convertListCCYRate(
			List<CurrencyRate> listCCYRate) {
		List<OandaCurrencyRates> listResult = new ArrayList<OandaCurrencyRates>();
		for (CurrencyRate ccyRate : listCCYRate) {
			listResult.add(convertCCYRate(ccyRate));
		}
		return listResult;
	}
}
