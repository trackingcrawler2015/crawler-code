/**
 * 
 */
package spiders;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import model.CCY;

import org.apache.commons.lang.StringUtils;
import org.apache.ibatis.session.SqlSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import utils.FileUtils;

import com.google.gson.Gson;

import dao.A02Dao;

/**
 * @author thi
 *
 */
public class A02 extends SpiderBase {
	A02Dao daoContent;
	static Logger logger = LogManager.getLogger(A02.class.getName());

	public A02() {
		daoContent = new A02Dao();
	}

	@Override
	public void getData() {
		Document doc = getDocument("http://www.oanda.com/currency/historical-rates/");
		String script = doc.select("script").toString();
		String ok = StringUtils
				.substringBetween(script, "'currencies':", ",\n");
		console(ok);
		Gson gson = new Gson();
		CCY[] listCCY = gson.fromJson(ok, CCY[].class);
		List<CCY> listOldCCY = getOldListCCY();
		List<CCY> listNewCCY = new ArrayList<CCY>();
		for (CCY ccy : listCCY) {
			if (ccy.getValue() != null) {
				listNewCCY.add(ccy);
				boolean existed = false;
				for (CCY oldCCY : listOldCCY) {
					if (ccy.equals(oldCCY)) {
						existed = true;
						break;
					}
				}
				if (existed == false) {
					ccy.setType(1);
				} else {
					ccy.setType(0);
				}
			}
		}

		System.out.println(listNewCCY.size());

		for (CCY ccy : listNewCCY) {
			if (daoContent.checkReqExisted(ccy) == false) {
				daoContent.createCCY(ccy);
			}
		}

	}

	public List<CCY> getOldListCCY() {
		List<CCY> listCCY = new ArrayList<CCY>();
		listCCY = daoContent.getListCCY();
		if (listCCY == null || listCCY.isEmpty()) {
			listCCY = getListCCYFromTxt();
		}
		return listCCY;
	}

	public List<CCY> getListCCYFromTxt() {
		List<CCY> listCCY = new ArrayList<CCY>();
		listCCY = daoContent.getListCCY();
		Scanner s = null;
		try {

			s = new Scanner(new BufferedReader(new FileReader(FileUtils.getJarDir() + "/resources/data/currency.txt")));

			while (s.hasNext()) {
				CCY ccy = new CCY();
				ccy.setValue(s.next());
				listCCY.add(ccy);
			}

		} catch (Exception e) {
			logger.error(e.getMessage());

		} finally {
			if (s != null) {
				s.close();
			}
		}
		return listCCY;
	}

	@Override
	public void createContentDB() {
		daoContent.createTables();

	}

	@Override
	public void exportData() {
		// TODO Auto-generated method stub
		List<CCY> listCCY = daoContent.getListCCY();

		
	}
}
