package spiders;

import java.io.File;

import model.settings.MailSetting;
import model.settings.Setting;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import utils.FileUtils;
import common.SettingCommon;
import common.SettingMail;

/**
 * @author thi
 *
 */
public class ApplicationInit {
	final static Logger logger = LogManager.getLogger(ApplicationInit.class);
	static Configuration config;

	static Setting setting;
	static MailSetting mailSetting;

	public static void loadSettings() {
		loadGlogalSettings();
	}

	public static void loadGlogalSettings() {
		try {
			config = new PropertiesConfiguration(
					FileUtils.getSettingFile("config"));
			setting = SettingCommon.getSpiderSetting(config);
			config = new PropertiesConfiguration(
					FileUtils.getSettingFile("mail"));
			mailSetting = SettingMail.getMailSetting(config);

		} catch (Exception e) {
			logger.error(e.getMessage());

		}

	}

	public static void main(String[] args) {
		loadSettings();
	}
}
