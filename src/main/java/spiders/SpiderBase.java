package spiders;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import model.crawler.RequestFail;
import model.crawler.Spider;
import model.crawler.SpiderInstance;
import model.parameters.CommandParameters;
import model.settings.Concurrent;
import model.settings.Connection;
import model.settings.MailSetting;
import model.settings.RequestHeader;
import model.settings.Setting;
import model.settings.SpiderInfo;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import utils.DateUtils;
import utils.FileUtils;
import utils.SendMailUtils;
import common.SettingCommon;
import dao.BaseDao;
import dao.SpiderDao;

public abstract class SpiderBase {
	public String spiderName;
	Configuration config;
	String settingPath;
	static Logger log = LogManager.getLogger(SpiderBase.class.getName());
	Setting setting;
	Concurrent concurrent;
	Connection con;
	RequestHeader reqHeader;
	SpiderInfo spiderInfo;

	String startURL;
	String currentURL;
	String queryString;
	SpiderDao dao;
	Spider spider;
	SpiderInstance spiderInstance;
	long startTime;
	long endTime;
	long interval;
	int success;
	int fail;
	MailSetting mailSetting;
	String dbPath;
	CommandParameters commandParams;

	public enum SpiderInstanceStatus {
		NEW, DONE
	};

	SpiderBase() {

	}

	public void initSpider() {
		try {
			//log.info("test agdfdfdf");
			settingPath = FileUtils.getSettingFile(this.getClass()
					.getSimpleName());

			File f = new File(settingPath);
			if (f != null && f.exists() == true) {
				config = new PropertiesConfiguration(settingPath);
			}

			spiderName = this.getClass().getSimpleName();
			dao = new SpiderDao();

			loadSettings();

			concurrent = setting.getConcurrent();
			con = setting.getConnection();
			reqHeader = setting.getRequestHeader();
			spiderInfo = setting.getSpider();
			startURL = spiderInfo.getStartURL() + "?"
					+ spiderInfo.getQueryString();
			currentURL = startURL;
			spider = insertSpider();
			// insert spider instance
			spiderInstance = insertSpiderInstance(spider);
			dbPath = "data" + "/" + spiderName;
			// f = new File(dbPath);
			/*
			 * f = FileUtils.getFileResource(this, dbPath); if (f.exists() ==
			 * false) { f.createNewFile(); }
			 */

			createContentDB();
		} catch (Exception e) {
			log.error(e.getMessage());

		}
	}

	public abstract void createContentDB();

	public Spider insertSpider() {
		// check spider existed
		boolean existed = dao.checkSpiderExisted(spiderName);
		Spider spider = new Spider();
		spider.setName(spiderName);
		spider.setStartURL(startURL);
		spider.setCreateTime(DateUtils.getTimeInString(new Date()));
		if (existed == false) {
			dao.createSpider(spider);
		}
		return spider;

	}

	public void insertReqFail(BaseDao baseDao) {
		RequestFail reqFail = new RequestFail();
		reqFail.setSpiderID(spider.getId());
		reqFail.setSpiderInstanceID(spiderInstance.getId());
		reqFail.setCreateTime(DateUtils.getTimeInString(new Date()));
		reqFail.setUrlReq(currentURL);
		baseDao.insertReqFail(reqFail);

	}

	public List<RequestFail> getListReqFail(BaseDao baseDao) {
		return baseDao.getListReqFailed();
	}

	public void deleteReqFail(BaseDao baseDao, RequestFail reqFail) {
		baseDao.deleteReqFail(reqFail);

	}

	public SpiderInstance insertSpiderInstance(Spider spider) {
		SpiderInstance spiderInst = new SpiderInstance();
		int spiderID = dao.getSpiderIDByName(spider.getName());
		spider.setId(spiderID);
		spiderInst.setSpiderID(spiderID);
		spiderInst.setStartTime(DateUtils.getTimeInString(new Date()));
		spiderInst.setStatus(SpiderInstanceStatus.NEW.ordinal());
		boolean existed = dao.checkReqExisted(currentURL, spiderID);
		if (existed == false) {
			dao.createSpiderInstance(spiderInst);
		}
		return spiderInst;
	}

	public void updateSpiderInstance(SpiderInstance spiderInst) {
		dao.updateSpiderInstance(spiderInst);
	}

	public void loadSettings() {
		ApplicationInit.loadSettings();
		mailSetting = ApplicationInit.mailSetting;

		//File f = FileUtils.getFileResource(this, settingPath);
		File f = new File(settingPath);
		if (f != null && f.exists() == true) {
			setting = SettingCommon.getSpiderSetting(config);
			SettingCommon.mergeSetting(setting, ApplicationInit.setting);
		} else {
			setting = ApplicationInit.setting;
		}
	}

	public Document getDocument(String url) {
		Document doc = null;
		int delay = con.getDelay();

		for (int i = 0; i < con.getTimes(); i++) {
			try {
				if (delay != -1) {
					Thread.sleep(delay);
				}

				doc = connect(url).get();
				break;
			} catch (Exception e) {
				log.error(e.getMessage() + ":" + url);
			}
		}
		return doc;

	}

	public org.jsoup.Connection connect(String url) {
		org.jsoup.Connection con = null;
		con = Jsoup.connect(url);
		if (reqHeader.getReferer() != null) {
			con.referrer(reqHeader.getReferer());
		}
		if (reqHeader.getUserAgent() != null) {
			con.userAgent(reqHeader.getUserAgent());
		}

		if (reqHeader.isIgnoreContentType() == true) {
			con.ignoreContentType(true);
		}

		if (reqHeader.getAcceptLanguage() != null) {
			con.header("Accept-Language", reqHeader.getAcceptLanguage());
		}

		if (reqHeader.getAcceptEncoding() != null) {
			con.header("Accept-Encoding", reqHeader.getAcceptEncoding());
		}

		if (reqHeader.getConnectionType() != null) {
			con.header("Connection", reqHeader.getConnectionType());
		}

		if (reqHeader.getxRequestedWith() != null) {
			con.header("X-Requested-With", reqHeader.getxRequestedWith());
		}

		if (this.con.getTimeout() != 0) {
			con.timeout(this.con.getTimeout());
		}

		return con;

	}

	public void updateSpiderInst() {
		spiderInstance.setEndTime(DateUtils.getTimeInString(new Date()));
		spiderInstance.setStatus(SpiderInstanceStatus.DONE.ordinal());
		spiderInstance.setTotalTime(DateUtils.getIntervalTime(
				spiderInstance.getStartTime(), spiderInstance.getEndTime()));
		spiderInstance.setSuccess(success);
		spiderInstance.setFail(fail);
		dao.updateSpiderInstance(spiderInstance);

	}

	public void run() {
		initSpider();
		getData();
		updateSpiderInst();
		nofify();
		exportData();
	}

	public void nofify() {
		String msg = "";
		if (mailSetting.isNotifyFail()) {
			msg = String.format(mailSetting.getNotifyDoneMessage(),
					spider.getStartURL(), spiderInstance.getSuccess(),
					spiderInstance.getFail());
			SendMailUtils.sendMail(mailSetting,
					mailSetting.getNotifyFailSubject(), msg);
		}

		if (mailSetting.isNotifyDone()) {
			msg = String.format(mailSetting.getNotifyDoneMessage(),
					spider.getStartURL(), spiderInstance.getSuccess(),
					spiderInstance.getFail());
			SendMailUtils.sendMail(mailSetting,
					mailSetting.getNotifyDoneSubject(), msg);
		}
	}

	public abstract void getData();

	public abstract void exportData();

	public void console(String msg) {
		System.out.println(msg);
	}

}
