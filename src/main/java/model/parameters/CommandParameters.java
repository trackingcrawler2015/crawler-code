package model.parameters;

import com.beust.jcommander.Parameter;

public class CommandParameters {

	@Parameter(names = "-request", description = "Request type(GET_CCY_LIST=1, RUN_NEW_CCY=2, RUN_OLD_CYY=3,RUN_DAILY_CYY=4)")
	private int request;

	public int getRequest() {
		return request;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	@Parameter(names = "-startDate", description = "start date")
	private String startDate;
	@Parameter(names = "-endDate", description = "end date")
	private String endDate;
}
