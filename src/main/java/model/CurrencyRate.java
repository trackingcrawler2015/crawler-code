package model;

import com.j256.ormlite.field.DatabaseField;

public class CurrencyRate {
	@DatabaseField(generatedId = true)
	private int id;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public double getRate() {
		return rate;
	}
	public void setRate(double rate) {
		this.rate = rate;
	}
	@DatabaseField
	private String date;
	@DatabaseField
	private double rate;
	public String getQuoteCurrency() {
		return quoteCurrency;
	}
	public void setQuoteCurrency(String quoteCurrency) {
		this.quoteCurrency = quoteCurrency;
	}
	public String getBaseCurrency() {
		return baseCurrency;
	}
	public void setBaseCurrency(String baseCurrency) {
		this.baseCurrency = baseCurrency;
	}
	@DatabaseField
	String quoteCurrency;
	@DatabaseField
	String baseCurrency;
	
	public String toString() {
		return baseCurrency+","+quoteCurrency+","+date+","+rate;
	}
}
