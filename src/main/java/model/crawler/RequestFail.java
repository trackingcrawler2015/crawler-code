package model.crawler;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "request_fail_list")
public class RequestFail {
	@DatabaseField(generatedId = true)
	private int id;

	public int getId() {
		return id;
	}

	public int getSpiderID() {
		return spiderID;
	}

	public void setSpiderID(int spiderID) {
		this.spiderID = spiderID;
	}

	public String getUrlReq() {
		return urlReq;
	}

	public void setUrlReq(String urlReq) {
		this.urlReq = urlReq;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getSpiderInstanceID() {
		return spiderInstanceID;
	}

	public void setSpiderInstanceID(int spiderInstanceID) {
		this.spiderInstanceID = spiderInstanceID;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	@DatabaseField(columnName = "spider_id")
	private int spiderID;
	@DatabaseField(columnName = "url_request")
	private String urlReq;
	@DatabaseField(columnName = "status")
	private int status;
	@DatabaseField(columnName = "spider_instance_id")
	private int spiderInstanceID;
	@DatabaseField(columnName = "create_time")
	private String createTime;

	public RequestFail() {

	}

}
