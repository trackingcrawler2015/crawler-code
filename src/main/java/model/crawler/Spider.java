/**
 * 
 */
package model.crawler;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * @author thi
 *
 */
@DatabaseTable(tableName = "spider")
public class Spider {
	@DatabaseField( generatedId=true)
	private int id;
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@DatabaseField
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getStartURL() {
		return startURL;
	}

	public void setStartURL(String startURL) {
		this.startURL = startURL;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	@DatabaseField
	private String desc;
	@DatabaseField(columnName = "start_url")
	private String startURL;
	@DatabaseField(columnName = "create_time")
	private String createTime;

	public Spider() {

	}

}
