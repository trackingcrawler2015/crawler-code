package model.crawler;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "spider_instance")
public class SpiderInstance {
	@DatabaseField( generatedId=true)
	private int id;
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getSpiderID() {
		return spiderID;
	}

	public void setSpiderID(int spiderID) {
		this.spiderID = spiderID;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public int getTotalTime() {
		return totalTime;
	}

	public void setTotalTime(int totalTime) {
		this.totalTime = totalTime;
	}

	public String getLastURLReq() {
		return lastURLReq;
	}

	public void setLastURLReq(String lastURLReq) {
		this.lastURLReq = lastURLReq;
	}

	public int getSuccess() {
		return success;
	}

	public void setSuccess(int success) {
		this.success = success;
	}

	public int getFail() {
		return fail;
	}

	public void setFail(int fail) {
		this.fail = fail;
	}

	@DatabaseField(columnName = "spider_id")
	private int spiderID;
	@DatabaseField(columnName = "start_time")
	private String startTime;
	@DatabaseField(columnName = "end_time")
	private String endTime;
	@DatabaseField(columnName = "total_time")
	private int totalTime;
	@DatabaseField(columnName = "last_url_request")
	private String lastURLReq;
	@DatabaseField(columnName = "success")
	private int success;
	@DatabaseField(columnName = "fail")
	private int fail;
	@DatabaseField(columnName = "status")
	private int status;
	

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public SpiderInstance() {

	}
}
