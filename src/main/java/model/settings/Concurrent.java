package model.settings;

public class Concurrent {
	// WARNING: We do not recommend to using any value higher than 5 if you are
	// not crawling your own domain,
	// as you may be blocked by the owner of the site.
	private int requestPerIP;
	public int getRequestPerIP() {
		return requestPerIP;
	}
	public void setRequestPerIP(int requestPerIP) {
		this.requestPerIP = requestPerIP;
	}
	public int getRequestPerDomain() {
		return requestPerDomain;
	}
	public void setRequestPerDomain(int requestPerDomain) {
		this.requestPerDomain = requestPerDomain;
	}
	private int requestPerDomain;

}
