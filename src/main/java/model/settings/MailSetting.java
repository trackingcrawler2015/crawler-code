package model.settings;

public class MailSetting {
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isAuth() {
		return auth;
	}

	public void setAuth(boolean auth) {
		this.auth = auth;
	}

	public boolean isStarttls() {
		return starttls;
	}

	public void setStarttls(boolean starttls) {
		this.starttls = starttls;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public boolean isNotifyFail() {
		return notifyFail;
	}

	public void setNotifyFail(boolean notifyFail) {
		this.notifyFail = notifyFail;
	}

	public String getNotifyFailSubject() {
		return notifyFailSubject;
	}

	public void setNotifyFailSubject(String notifyFailSubject) {
		this.notifyFailSubject = notifyFailSubject;
	}

	public boolean isNotifyDone() {
		return notifyDone;
	}

	public void setNotifyDone(boolean notifyDone) {
		this.notifyDone = notifyDone;
	}

	public String getNotifyDoneSubject() {
		return notifyDoneSubject;
	}

	public void setNotifyDoneSubject(String notifyDoneSubject) {
		this.notifyDoneSubject = notifyDoneSubject;
	}

	private String userName;
	private String password;
	private boolean auth;
	private boolean starttls;
	private String host;
	private String port;
	private String from;
	private String to;
	private boolean notifyFail;
	private String notifyFailSubject;
	private boolean notifyDone;
	private String notifyDoneSubject;
	public String getNotifyFailMessage() {
		return notifyFailMessage;
	}

	public void setNotifyFailMessage(String notifyFailMessage) {
		this.notifyFailMessage = notifyFailMessage;
	}

	public String getNotifyDoneMessage() {
		return notifyDoneMessage;
	}

	public void setNotifyDoneMessage(String notifyDoneMessage) {
		this.notifyDoneMessage = notifyDoneMessage;
	}

	private String notifyFailMessage;
	private String notifyDoneMessage;

}
