package common;

public class RequestConstants {
	public static final int GET_CCY_LIST=1;
	public static final int RUN_NEW_CCY=2;
	public static final int RUN_OLD_CYY=3;
	public static final int RUN_DAILY_CYY=4;
	public static final int RUN_REQ_CCY_FAIL=5;
}
