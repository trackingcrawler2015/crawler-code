/**
 * 
 */
package main;

import model.parameters.CommandParameters;

import com.beust.jcommander.JCommander;

import common.RequestConstants;
import spiders.A01;
import spiders.A02;
import spiders.SpiderBase;

/**
 * @author thi
 *
 */
public class crawler {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		CommandParameters params = new CommandParameters();
		new JCommander(params, args);
		int request = params.getRequest();
		SpiderBase spider = null;
		
		switch (request) {
		case RequestConstants.GET_CCY_LIST:
			spider = new A02();
			break;
		case RequestConstants.RUN_DAILY_CYY:
			spider = new A01(RequestConstants.RUN_DAILY_CYY, params);
			break;
		case RequestConstants.RUN_NEW_CCY:
			spider = new A01(RequestConstants.RUN_NEW_CCY, params);
			break;
		case RequestConstants.RUN_OLD_CYY:
			spider = new A01(RequestConstants.RUN_OLD_CYY, params);
			break;
		case RequestConstants.RUN_REQ_CCY_FAIL:
			spider = new A01(RequestConstants.RUN_REQ_CCY_FAIL, params);
			break;
		default:
			break;
		}

		if (spider != null) {
			spider.run();
		}
	}

}
